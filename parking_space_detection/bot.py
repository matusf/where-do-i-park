import os
import logging
import threading
from io import BytesIO
from argparse import ArgumentParser

from PIL import Image
from telegram.ext import Updater, CommandHandler
from telegram.error import (
    TelegramError,
    Unauthorized,
    BadRequest,
    TimedOut,
    ChatMigrated,
    NetworkError,
)

from .detector import Detector


HELP_MESSAGE = """
Hi, I detect free parking spaces!
Here are available commands:

/help - display this help message
/park - get info about available parking spaces
"""

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def help_command(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text(HELP_MESSAGE)


def park_command(detector, update, context):
    """Send photo of parking space along with status message to user when the
    command /park is issued.
    """
    image, is_free = detector.report()
    if image is not None:
        bio = BytesIO()
        img = Image.fromarray(image)
        img.save(bio, "jpeg")
        bio.seek(0)
        if is_free:
            update.message.reply_text("Free space available!")
        else:
            update.message.reply_text("No free space")

        update.message.reply_photo(bio)
    else:
        update.message.reply_text("Setting up image recognition")


def error_callback(update, context):
    """Handle telegram related errors"""
    try:
        raise context.error
    except (
        Unauthorized,
        BadRequest,
        TimedOut,
        NetworkError,
        ChatMigrated,
        TelegramError,
    ) as e:
        logger.error(str(e), exc_info=True)


def main():
    parser = ArgumentParser(description="Available telegram commands: /help /park")
    parser.add_argument(
        "-t",
        dest="token",
        metavar="TELEGRAM_TOKEN",
        action="store",
        help="telegram bot token (env: TELEGRAM_TOKEN)",
        default=os.getenv("TELEGRAM_TOKEN"),
        required=not os.getenv("TELEGRAM_TOKEN"),
    )
    parser.add_argument(
        "-i",
        dest="input",
        metavar="VIDEO_SOURCE",
        action="store",
        help="video source input (env: VIDEO_SOURCE)",
        default=os.getenv("TELEGRAM_TOKEN"),
        required=not os.getenv("VIDEO_SOURCE"),
        type=lambda i: int(i) if i.isnumeric() else i,
    )

    args = parser.parse_args()

    updater = Updater(args.token, use_context=True)
    detector = Detector(args.input)
    t = threading.Thread(target=detector.run)
    t.start()

    dp = updater.dispatcher
    dp.add_handler(CommandHandler("start", help_command))
    dp.add_handler(CommandHandler("help", help_command))
    dp.add_handler(CommandHandler("park", lambda u, c: park_command(detector, u, c)))
    dp.add_error_handler(error_callback)

    logger.info("Starting polling")
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
