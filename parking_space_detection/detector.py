import sys

import cv2
import torch
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from detectron2.structures import Boxes, pairwise_iou
from detectron2.utils.logger import setup_logger


WHITE = (255, 255, 255)
RED = (0, 0, 255)
GREEN = (0, 255, 0)
CAR_CLASSES = (2, 7)
# How many frames needs to be space free to report it as free
FRAMES_TRESHOLD = 2
OCUPIED_TRESHOLD = 0.15
FONT = cv2.FONT_HERSHEY_DUPLEX


class Detector:
    def __init__(self, video_input):
        self.video_input = video_input
        self.cfg = None
        self.image = None
        self.previous_boxes = None
        self.is_free = False

    def configure(self):
        self.cfg = get_cfg()

        # Raspberry Pi does not support CUDA
        self.cfg.MODEL.DEVICE = "cpu"
        self.cfg.merge_from_file(
            model_zoo.get_config_file(
                "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
            )
        )
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
        self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
            "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
        )

    def report(self):
        return self.image, self.is_free

    def run(self):
        setup_logger()
        self.configure()
        video_capture = cv2.VideoCapture(self.video_input)
        predictor = DefaultPredictor(self.cfg)

        while video_capture.isOpened():
            success, frame = video_capture.read()
            if not success:
                break

            instances = predictor(frame)["instances"].to("cpu")
            fields = instances.get_fields()

            cars = [
                box.tolist()
                for box, class_type in zip(fields["pred_boxes"], fields["pred_classes"])
                if class_type in CAR_CLASSES
            ]

            if self.previous_boxes is None:
                self.previous_boxes = Boxes(torch.Tensor(cars))
                continue

            current_boxes = Boxes(torch.Tensor(cars))
            iou = pairwise_iou(self.previous_boxes, current_boxes)

            is_free = False
            for overlap, box in zip(iou, self.previous_boxes):
                max_overlap = torch.max(overlap)

                x1, y1, x2, y2 = box
                if max_overlap < OCUPIED_TRESHOLD:
                    cv2.rectangle(frame, (x1, y1), (x2, y2), GREEN, 3)
                    is_free = True
                else:
                    cv2.rectangle(frame, (x1, y1), (x2, y2), RED, 3)

                cv2.putText(
                    frame, f"{max_overlap:0.2}", (x1 + 6, y2 - 6), FONT, 1, WHITE,
                )

            self.is_free = is_free
            self.image = frame[:, :, ::-1]

        video_capture.release()


if __name__ == "__main__":
    d = Detector(sys.argv[1] if len(sys.argv) > 1 else "data/parking1.mp4")
    d.run()
