from setuptools import setup, find_packages


with open("README.md", encoding="utf-8") as f:
    long_description = f.read()

with open("requirements.txt", encoding="utf-8") as f:
    requirements = list(map(str.strip, f.readlines()))[2:]  # skip find links urls

setup(
    name="where-do-i-park",
    version="0.0.1",
    description="Bot detecting free parking spaces",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(exclude=("tests",)),
    install_requires=requirements,
    include_package_data=True,
    entry_points={
        "console_scripts": ["run-park-bot = parking_space_detection.bot:main"]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
