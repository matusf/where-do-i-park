# Where do I park

This project aims to create an easy to use solution for detection of free parking spaces using a low-end camera and Raspberry Pi.

It uses [OpenCV](https://opencv.org/) for image manipulation and utilises mask recursive convolutional neural networks for object detection and segmentation from [detectron2](https://github.com/facebookresearch/detectron2/) along with underlining [PyTorch](https://pytorch.org/) developed by Facebook Research.

## Installation

You can install this package easily using `pip`. The bot can be than run  by invoking `run-park-bot` command.

```txt
pip install \
    -f https://download.pytorch.org/whl/torch_stable.html \
    -f https://dl.fbaipublicfiles.com/detectron2/wheels/cpu/torch1.5/index.html \
    git+https://gitlab.com/matusf/where-do-i-park.git
```

## Help & Configuration

Bot can be configured using cli options or environment variables.

```txt
usage: run-park-bot [-h] -t TELEGRAM_TOKEN -i VIDEO_SOURCE

Available telegram commands: /help /park

optional arguments:
  -h, --help         show this help message and exit
  -t TELEGRAM_TOKEN  telegram bot token (env: TELEGRAM_TOKEN)
  -i VIDEO_SOURCE    video source input (env: VIDEO_SOURCE)
```
